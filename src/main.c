#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>

#define MALLOC_SIZE 128

static struct block_header *block_get_header(void *contents)
{
  return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

void test_alloc_region() {
    printf("Test - region allocation\n");
    struct block_header *block = heap_init(REGION_MIN_SIZE);
    assert(block);

    void *ptr = _malloc(MALLOC_SIZE);
    struct block_header *block1 = block_get_header(ptr);
    assert(block1->is_free != true);
    assert(block1->capacity.bytes == MALLOC_SIZE);

    heap_term();
}

void test_free_one() {
    printf("Test - free one block after three blocks allocated\n");
    struct block_header *block = heap_init(REGION_MIN_SIZE);
    assert(block);
    void *ptr = _malloc(MALLOC_SIZE);
    struct block_header *block1 = block_get_header(ptr);
    void *ptr1 = _malloc(MALLOC_SIZE);
    struct block_header *block2 = block_get_header(ptr1);
    void *ptr2 = _malloc(MALLOC_SIZE);
    struct block_header *block3 = block_get_header(ptr2);

    _free(block3);
    assert(block1->is_free != true);
    assert(block2->is_free != true);
    assert(block3->is_free == true);

    heap_term();
}

void test_free_two() {
    printf("Test - free two block after three blocks allocated\n");
    struct block_header *block = heap_init(REGION_MIN_SIZE);
    assert(block);
    void *ptr = _malloc(MALLOC_SIZE);
    struct block_header *block1 = block_get_header(ptr);
    void *ptr1 = _malloc(MALLOC_SIZE);
    struct block_header *block2 = block_get_header(ptr1);
    void *ptr2 = _malloc(MALLOC_SIZE);
    struct block_header *block3 = block_get_header(ptr2);

    _free(block3);
    assert(block1->is_free != true);
    assert(block2->is_free != true);
    assert(block3->is_free == true);

    _free(block2);
    assert(block1->is_free != true);
    assert(block2->is_free == true);
    assert(block3->is_free == true);

    heap_term();
}

void test_new_region_grow() {
    printf("Test - free two block after three blocks allocated\n");
    struct block_header *block = heap_init(REGION_MIN_SIZE);
    assert(block);
    void *ptr = _malloc(MALLOC_SIZE);
    struct block_header *block1 = block_get_header(ptr);

    assert(block1->capacity.bytes == REGION_MIN_SIZE);
    assert(size_from_capacity(block1->capacity).bytes > REGION_MIN_SIZE);

    heap_term();
}

int main() {
    test_alloc_region();
    test_free_one();
    test_free_two();
    test_new_region_grow();
    return 0;
}